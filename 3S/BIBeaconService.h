//
//  BIBeaconService.h
//  BeaconService
//
//  Created by Cornelius Rabsch on 13.10.14.
//  Copyright (c) 2015 BEACONinside GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class BIBeaconServiceConfig;

/* Beacon detection accuracy levels  */
typedef enum {
    // Beacons detected upon user interactions with the phone
    kBIBeaconServiceDetectionAccuracyLowest = 0,
    
    // Good compromise between location accuracy and energy consumption
    kBIBeaconServiceDetectionAccuracyOptimized,
    
    // Highest responsiveness to location changes, but highest resource consumption
    kBIBeaconServiceDetectionAccuracyHighest
} BIBeaconServiceDetectionAccuracy;



/*
 Tracking modes for analytics
 */
typedef enum {
    // Turn off event tracking for analytics purposes
    kBIBeaconServiceTrackingModeOff = 0,
    
    // Optimized tracking scheme
    kBIBeaconServiceTrackingModeOptimized,
    
    // Dispatch event updates as fast as possible
    kBIBeaconServiceTrackingModeImmediate
} BIBeaconServiceTrackingMode;



/*
 The Beaconinside beacon service library
 */
@interface BIBeaconService : NSObject

/*
 Init beacon service with API application token.
 If called with new token, account will be switched.
 
 The config can be used to add custom behaviors.
 */
+ (void)initWithToken:(NSString*)token;
+ (void)initWithToken:(NSString *)token andConfig:(BIBeaconServiceConfig*)config;

/*
 Safely terminate the beacon service (i.e. stop all beacon monitoring)
 */
+ (void)terminate;

/*
 Authorization of required permission (iOS only)
 */
+ (void)requestAuthorization;
+ (BOOL)isAuthorized;

/*
 Define custom behavior when local notification is tapped
 */
+ (void)handleNotification:(UILocalNotification*)notification;

/*
 Return account token
 */
+ (NSString*)token;

@end



#pragma mark - Configuration builder


@interface BIBeaconServiceConfig : NSObject


/*
 Designated initializer for configuration builder
 */
+ (BIBeaconServiceConfig*)defaultConfig;


/*
 Beta: Set beacon detection accuracy level
 */
- (void)setBeaconDetectionAccuracy:(BIBeaconServiceDetectionAccuracy)accuracy;


/*
 Set custom IDs for your own reporting and analytics tasks
 */
- (void)setCustomID1:(NSString*)identifier;
- (void)setCustomID2:(NSString*)identifier;


@end