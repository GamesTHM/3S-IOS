//
//  File.swift
//  3S
//
//  Created by Daniel Hahn on 17.06.15.
//  Copyright (c) 2015 Games@THM. All rights reserved.
//

import UIKit
import CoreLocation

class LoginController: UIViewController, UIWebViewDelegate, CLLocationManagerDelegate {
    // MARK: Properties
    
    @IBOutlet weak var mWebView: UIWebView!
    var manager: OneShotLocationManager?
    var locationManager: CLLocationManager?
    let uuidString = "F0018B9B-7509-4C31-A905-1A27D39C003C"
    let uuidStringLow = "f0018b9b-7509-4c31-a905-1a27d39c003c"
    let beaconIdentifier = "" //BEACONinside
    let beaconLayout = "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"
    var beaconRegion:CLBeaconRegion?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let requestURL = NSURL(string:"http://george.mnd.thm.de/main.html")
        let request = NSURLRequest(URL: requestURL!)
        
        //Load Webpage
        mWebView.loadRequest(request)
        mWebView.delegate = self
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidStartLoad(webView: UIWebView)
    {
    }
    func webViewDidFinishLoad(webView: UIWebView)
    {
        //updateUserPosition()
        startUpdatePosition()
        initBeacons()
    }
    func initBeacons()
    {
        //Set Beacon Settings
        let beaconUUID:NSUUID = NSUUID(UUIDString: self.uuidString)!
        self.beaconRegion = CLBeaconRegion(proximityUUID: beaconUUID, identifier: self.beaconIdentifier)
        //Init Beacon Location
        self.locationManager = CLLocationManager()
        if(locationManager!.respondsToSelector("requestAlwaysAuthorization")) {
            locationManager!.requestAlwaysAuthorization()
        }
        self.locationManager!.delegate = self
        self.locationManager!.pausesLocationUpdatesAutomatically = false
        self.locationManager!.startMonitoringForRegion(beaconRegion!)
        self.locationManager!.startRangingBeaconsInRegion(beaconRegion!)
        //        locationManager!.startUpdatingLocation()
    }
    func startUpdatePosition()
    {
        let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
        dispatch_async(queue) {
            while(true)
            {
                dispatch_async(dispatch_get_main_queue(),
                {
                    self.updateUserPosition()
                    //self.initBeacons()
                })
                sleep(120)
            }
            /* do something with someData */
        }
    }
    func updateUserPosition()
    {
        manager = OneShotLocationManager()
        manager!.fetchWithCompletion {location, error in
            
            // fetch location or an error
            if let loc = location {
                var script = "setUserLocation('"
                script += String(format: "%.8f",loc.coordinate.latitude)
                script += "','"
                script += String(format: "%.8f",loc.coordinate.longitude)
                script += "')"
                
                self.mWebView.stringByEvaluatingJavaScriptFromString(script)
                
                //NSLog("%@", String(format: "%.8f",loc.coordinate.latitude))
                //NSLog("%@", String(format: "%.8f",loc.coordinate.longitude))
            } else if let err = error {
                print(err.localizedDescription)
            }
            self.manager = nil
        }
    }
    func locationManager(manager: CLLocationManager!,
        didRangeBeacons beacons: [CLBeacon]!,
        inRegion region: CLBeaconRegion!) {
            
            if(beacons.count > 0) {
                for beacon in beacons
                {
                    let nearestBeacon:CLBeacon = beacon
                    
                    var script = "addLocation('"
                    script += String(self.uuidStringLow)
                    script += "','"
                    script += String(nearestBeacon.major)
                    script += "','"
                    script += String(nearestBeacon.minor)
                    script += "')"
                    
                    self.mWebView.stringByEvaluatingJavaScriptFromString(script)
                    
                    NSLog("%@", script)
                    NSLog("%@", nearestBeacon.minor)
                    NSLog("%@", nearestBeacon.major)
                }
                
                
            }
            //self.locationManager!.stopMonitoringForRegion(self.beaconRegion!)
            //self.locationManager!.stopRangingBeaconsInRegion(self.beaconRegion!)
            
            //sendLocalNotificationWithMessage(message)
    }

}
